### GraphQL tutorial
1. console: ```node server.js```
2. browser's ```addres: localhost:4000/graphql```
3. interface's input
```
mutation {
  editUser(id:"1", firstName:"Alex123") {
    id
    firstName
    age
  }
}
```
4. interface's output:
```
{
  "data": {
    "editUser": {
      "id": "1",
      "firstName": "Alex",
      "age": 321
    }
  }
}
```
